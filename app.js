'use strict';

const path = require('path');
const dotenv = require('dotenv').config({'silent': true});
const debug = require('debug')('n-struct:app');
const express = require('express');
const favicon = require('serve-favicon');
const config = require('./config');
const responses = require('./responses');
const middleware = require('./middleware');
const routes = require('./routes');

/**
 * Load the environment variables
 */
if (!dotenv) {
  debug('Unable to load the necessary variables from the .env file.');
  process.exit(1);
}

const app = express();

/**
 * //
 */
app.disable('etag');

/**
 * Configure the application proxy settings
 */
app.set('trust proxy', config.get('app.behindProxy'));

/**
 * Configure the application port
 */
app.set('port', function(val) {
  var port = parseInt(val, 10);
  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    return port;
  }
  return false;
}(config.get('app.port', 443)));

/**
 * Setup pretty responses
 */
if (config.get('app.prettify', false)) {
  app.locals.pretty = true;
  app.set('json spaces', 2);
}

/**
 * Serve the favicon
 */
// app.use(favicon(path.join(__dirname, 'public', 'dist', 'favicon.ico')));

/**
 * Apply global middleware
 */
middleware.global(app);

/**
 * Apply the router
 */
app.use(routes);

/**
 * Apply 404 catch
 */
app.use(function(request, response, next) {
  next(new responses.NotFoundError); // Resource not found
});

/**
 * Apply a route response handler
 */
app.use(responses.handler);

module.exports = app;
