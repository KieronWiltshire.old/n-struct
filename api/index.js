'use strict';

const _ = require('lodash');
const path = require('path');
const debug = require('debug')('n-struct:api');
const responses = require('../responses');

/**
 * Api
 */
module.exports = new function() {
  var self = this;

  /**
   * Endpoints
   */

  /**
   * Conform's a request to api method parameters.
   *
   * @param {Object} request
   * @returns {Object} data
   */
  self.conform = function(request) {
    var error = null;
    var object = request.body;
    var options = _.extend({
      'include': [],
      'limit': 1000,
      'offset': 0,
    }, request.file, request.query, request.params, {
      'context': request.context,
      'authorization': request.authorization
    });

    // Attempt to parse the meta parameter if one was passed
    if (options.meta && options.meta.toLowerCase() === 'true') {
      options.meta = true;
    } else {
      options.meta = false;
    }

    // If the request contained a where clause, delete it.
    if (options.where) {
      delete options.where;
    }

    // allow pagination on requests
    options['limit'] = (options['limit'] > 1000) ? 1000 : ((options['limit'] <= 0) ? 1 : parseInt(options['limit']));
    options['offset'] = (options['offset'] < 0) ? 0 : parseInt(options['offset']);

    // If order isn't an array, then we resolve
    // the conflict by appending the ASC value directly
    var order = [];
    if (Array.isArray(options.order)) {
      // If the order lenght is more than 2, then error
      if (options.order.length > 2) {
        error = new responses.BadRequestError;
      } else {
        if (options.order[0].toUpperCase() === 'DESC' || options.order[0].toUpperCase() === 'ASC') {
          var first = options.order[0];
          var second = options.order[1];

          options.order[0] = second;
          options.order[1] = first;
        }

        order.push(options.order);
      }
    } else {
      // If an order hasn't been specified, then we
      // specify a default one
      if (!options.order) {
        options.order = ['id', 'DESC'];
      } else {
        if (options.order.toUpperCase() === 'DESC' || options.order.toUpperCase() === 'ASC') {
          options.order = ['id', options.order];
        } else {
          options.order = [options.order, 'DESC'];
        }
      }

      order.push(options.order);
    }
    options.order = order;

    // If the request method body is empty, then we
    // only have the options (query & params)
    if (_.isEmpty(object) && request.method === 'GET') {
      object = options;
      options = {};
    }

    // return the conformed request data
    if (error) {
      return { error: error }
    } else {
      return { object: object, options: options };
    }
  };

  /**
   * An api http request handler.
   *
   * @param {Function} method
   * @returns {Function} handler
   */
  self.http = function(method) {
    return function handler(request, response, next) {
      var data = self.conform(request);

      if (data.error) {
        return next(data.error);
      } else {
        return method(data.object, data.options).then(function(value) {
          switch (request.method) {
            case 'POST':
              var status = 200;
              if (value && value.$options && value.$options.isNewRecord) {
                response.set({
                  'Location': request.originalUrl + '/' + value.id
                });
                status = 201;
              }
              return response.status(status).json(value);
            case 'DELETE':
              return response.status(204).end();
            default:
              return response.json(value || {});
          }
        }).catch(function(error) {
          debug(error);
          next(error);
        });
      }
    };
  }
};
