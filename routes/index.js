'use strict';

const _ = require('lodash');
const path = require('path');
const express = require('express');
const debug = require('debug')('n-struct:router');
const api = require('./api');
const responses = require('../responses');

var router = express.Router();

/**
 * Configure the api and plugin endpoints
 */
router.use('/api', api);

/**
 * Redirect non-existent api calls to the front-end
 */
router.use(function(request, response, next) {
  if (request.originalUrl.startsWith('/api')) {
    next(new responses.NotFoundError);
  } else {
    next();
  }
}, express.static(path.join(__dirname, '..', 'public')))

/**
 * Export the router
 */
module.exports = router;
