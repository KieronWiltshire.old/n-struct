'use strict';

const express = require('express');
const api = require('../api');
const middleware = require('../middleware').named;
const config = require('../config');

var router = express.Router();

module.exports = router;
