# Installation

- Clone the repository
- Rename `.env.example` to `.env`
- Change the configuration options in the `.env` file
- Execute the `npm install --production` command to install the dependencies

Run the `npm start` command in order to launch the server.
It is highly suggested that you use a tool like [forever][1] to help ensure that your server is continuously running.

#### Security

It is highly recommended that you secure any client accessing the end-points to help prevent [man in the middle][2] attacks, securing the connection between both parties is critical to the security of the application.

If you wish to secure the connection via a proxy, then you may do so by enabling the `APP_BEHIND_PROXY` option within the `.env` file.
You may also secure the connection directly, this can optionally be used with a proxy server and is enabled with the `APP_SECURE` option within the `.env` file. This will require you to include your `key.pem` and `certificate.pem` files within the root directory.

# Testing

The application utilizes [Mocha][3] and [SuperTest][4] to conduct it's unit tests.
In order to run the tests, you'll need to be able to run mocha directly from the cli.

- Follow the installation steps above
- Change the value of `NODE_ENV` option within the `.env` file to `testing`
- Execute the `npm install --dev` command to install the development dependencies

Run the `npm test` command in order to start executing the test cases.

---
# Debugging

The application will throw debug errors if something abnormal occurs. You may view these errors by enabling the debug options in your cli using the following command `DEBUG=n-struct:*`, if you're using a windows operating system then you can enable the debug options with this command instead `set debug=n-struct:*`.

[1]: https://github.com/foreverjs/forever
[2]: https://en.wikipedia.org/wiki/Man-in-the-middle_attack
[3]: https://github.com/mochajs/mocha
[4]: https://github.com/visionmedia/supertest
