"use strict";

const api = require('../api');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const helmet = require('helmet');
const cors = require('cors');
const debug = require('debug')('n-struct:middleware');

/**
 * Global Middleware
 */
module.exports.global = function(app) {
  if (process.env.NODE_ENV !== 'testing') {
    app.use(logger('dev'));
  }

  /**
   * ...
   */
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({'extended': true}));
  app.use(cookieParser());
  app.use(methodOverride('X-HTTP-Method-Override'));
  app.use(helmet());
  app.use(cors());
};

/**
 * Named Middleware
 */
module.exports.named = {
};
