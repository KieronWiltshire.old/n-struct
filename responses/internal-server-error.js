'use strict';

/**
 * Create an InternalServerError response
 *
 * @returns {Object} error
 */
function InternalServerError() {
  var self = this;

  self.type = this.name;
  self.status = 500;
  self.stack = new Error().stack;
  self.exceptions = [];

  self.exception = function(exception) {
    self.exceptions.push(exception);
    return self;
  };
}

InternalServerError.prototype = Object.create(require('./base-error').prototype);
InternalServerError.prototype.name = 'InternalServerError';

module.exports = InternalServerError;
