'use strict';

/**
 * Create a BadRequestError response
 *
 * @returns {Object} error
 */
function BadRequestError() {
  var self = this;

  self.type = this.name;
  self.status = 400;
  self.stack = new Error().stack;
  self.exceptions = [];

  self.exception = function(exception) {
    self.exceptions.push(exception);
    return self;
  };
}

BadRequestError.prototype = Object.create(require('./base-error').prototype);
BadRequestError.prototype.name = 'BadRequestError';

module.exports = BadRequestError;
