'use strict';

/**
 * Create a NotFoundError response
 *
 * @returns {Object} error
 */
function NotFoundError() {
  var self = this;

  self.type = this.name;
  self.status = 404;
  self.stack = new Error().stack;

  self.exceptions = [];

  self.exception = function(exception) {
    self.exceptions.push(exception);
    return self;
  };
}

NotFoundError.prototype = Object.create(require('./base-error').prototype);
NotFoundError.prototype.name = 'NotFoundError';

module.exports = NotFoundError;
